# Environment Setup

To get the app ready to run, first ensure you're running on the same node version as the rest of the team and run the following command to run NODE v14.16.0
```
nvm use
```
If you don't have nvm, download here: https://github.com/nvm-sh/nvm#installing-and-updating . Otherwise, just ensure you're running 14.16.0

## Install Dependencies
Next install the dependencies using the standard npm install commnad
```
npm i
```

## Run the app
In the project directory, you can run:

```
yarn start
```
Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### Design decisions
Decided to go with React, Redux, Redux Sagas, Formik and Material UI. Reasons below:

- React - Good community support, virtual dom, one way data binding, 
- Redux - State management of the app if we may be using the parts reducer in a different part of the app.... Might be needed for example to show a diagram or other components related to Parts that will use the same info.
- Redux-Saga - Used mostly to allow the app to scale vs thunk.
- Formik - almost standard to create forms in React, allowed for simple form event handling that was necessary to dispatch info to PUT request
- Material UI - Rather then create custom styles and HTML, for fast prototypes, Material UI allows for the use of pre-existing components that allow for decent good looking design. Used Grid, Pagination and Input components.

### Page Component/App Breakdown
- Simple router was created to show the HOME page on the `/` route.
- This Home page then loads the Pagination component.
- In the Pagination component we are calling an action that will call the API w/ the default query parameter of 1. The Pagination Material UI component (loaded inside the custom built pagination component) is then updated w/ info from the HEADERS from the API to show appropriate pages amount.
- With the info from the API, we display the information in the pagination component. Each "row" is essentially it's own form since it has its own submit button.
- Each submit button sends a PUT request in the browser that today doesn't do anything.
