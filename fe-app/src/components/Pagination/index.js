import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getParts, savePartAmt} from '../../store/actions';
import { useFormik } from 'formik';
import Pagination from '@material-ui/lab/Pagination';
import {Grid, OutlinedInput, Button} from '@material-ui/core';



function PaginationComponent() {
  const dispatch = useDispatch();
  const parts  = useSelector(state => state.parts);
  const goGetParts = (pg = 1) => dispatch(getParts(pg));
  const savePartInfo = (partId, partAmt) => dispatch(savePartAmt(partId, partAmt));

  useEffect(() => {
    goGetParts();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const handleChangez = (e, pgNum) => {
    goGetParts(pgNum);
  };

  const CustomFormik = ({val}) => {
    const initialVal = {};
    initialVal[`part_quantity_${val.id}`] = val.quantity;
  
    const formik = useFormik({
      initialValues: initialVal,
      onSubmit: (values, {setSubmitting}) => {
        const partId = Object.keys(values)[0].replace('part_quantity_', '');
        const partAmt = Object.values(values)[0];
        savePartInfo(partId, partAmt);
        setSubmitting(false);
      },
    });
  
    return (
      <div>
        <form onSubmit={formik.handleSubmit}>
          <Grid container item xs={12} spacing={2}>
            <Grid item xs={4}>
              <label htmlFor={`part_quantity_${val.id}`}>{val.part_file.file_name}</label>
            </Grid>
            <Grid item xs={4}>
            <OutlinedInput
              variant="outlined"
              type="text"
              name={`part_quantity_${val.id}`}
              value={formik.values[`part_quantity_${val.id}`]}
              onChange={formik.handleChange}
            />
            </Grid>
            <Grid item xs={4}>
              <Button variant="contained" color="primary" type="submit" disabled={formik.isSubmitting}>Submit</Button>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  };

  return (
    <div className="pagination">
      {parts?.parts?.data ? parts.parts.data.map((val, key) => {
        return <CustomFormik val={val} key={key}/>
      }): null}
  
      <Pagination
        onChange={handleChangez}
        count={parseInt(parts.partsInfo['total-pages']) || 1}
      />
    </div>
  );
}

export default PaginationComponent;
