import Home from './pages/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Container from '@material-ui/core/Container';

function App() {
  return (
    <Router>
      <Container maxWidth="md">
        <header>
          <h1>Fast Radius App</h1>
        </header>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
