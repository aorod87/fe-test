import partsReducer from "./reducers/parts";

export default function rootReducer(state = {}, action) {
  return {
    parts: partsReducer(state.parts, action)
  }
}