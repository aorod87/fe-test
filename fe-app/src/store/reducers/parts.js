import { GET_PARTS, GET_PARTS_SUCCESS } from '../constants';

const initialState = {
  parts: [],
  partsInfo: {
    'total-pages': 1
  },
  loading: false
}

export default function partsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_PARTS:
       return { ...state, loading: true };
     case GET_PARTS_SUCCESS:
       return {
         ...state,
         parts: action.data.data,
         partsInfo: action.data.headers,
         loading: false
       }
    default:
       return state;
  }
}
