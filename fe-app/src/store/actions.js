import { GET_PARTS, SAVE_PARTS } from './constants';

export const getParts = pg => {
  return {
    type: GET_PARTS,
    payload: {
      pg
    }
  }
}

export const savePartAmt = (id, amt) => {
  return {
    type: SAVE_PARTS,
    payload: {
      id,
      amt
    }
  };
}