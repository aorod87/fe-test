import { put, takeLatest, } from 'redux-saga/effects'
import { GET_PARTS, GET_PARTS_FAILED, GET_PARTS_SUCCESS, SAVE_PARTS, SAVE_PARTS_FAILED, SAVE_PARTS_SUCCESS } from '../constants';

const fetchParts = function* (action) {
  const pg = action?.payload?.pg ? action?.payload?.pg : 1;
  try {
    const headerPropsNeeded = ['page-number', 'per-page', 'total-entries', 'total-pages'];
    let headers = {};
    const resp = yield fetch(`http://localhost:5555/parts/?page=${pg}`)
    resp.headers.forEach((val, key) => {
      if (headerPropsNeeded.indexOf(key) >= 0) headers[key] = val;
    });
    const data = yield resp.json();
    yield put({ type: GET_PARTS_SUCCESS, data: {data, headers}});
  } catch (e) {
    yield put({ type: GET_PARTS_FAILED, message: e.message });
  }
}

const saveParts = function* (action) {
  try {
    const {id, amt} = action?.payload;
    const saveResp = yield fetch(`http://localhost:5555/parts/${id}`, {
      method: 'PUT',
      body: JSON.stringify({
        quantity: amt
      })
    })
    const data = yield saveResp.json();
    yield put({ type: SAVE_PARTS_SUCCESS, data });
  } catch (e) {
    yield put({ type: SAVE_PARTS_FAILED, message: e.message });
  }
}

export function* watchFetchParts() {
  yield takeLatest(GET_PARTS, fetchParts)
}

export function* watchSaveParts() {
  yield takeLatest(SAVE_PARTS, saveParts)
}