import React from 'react';
import Pagination from '../../components/Pagination';

function Home() {
  return (
    <div className="pg pg-home">
        <p>Welcome to your parts center</p>
        <Pagination />
    </div>
  );
}

export default Home;
